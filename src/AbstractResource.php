<?php

namespace Mdw\Resource;

use Interop\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AbstractResource
 *
 * Provides abstract functionality for API resource classes
 *
 * @package Mdw\Resource
 */
abstract class AbstractResource
{

    /**
     * @var \Interop\Container\ContainerInterface
     */
    protected $container;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $args;

    /**
     * @param \Interop\Container\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->init();
    }

    protected function init()
    {
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return mixed
     */
    public function __invoke(Request $request, Response $response, $args)
    {
        /*
         * We may need access to this, so let's keep them around
         */
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;

        $id = null;
        if (isset($args['id'])) {
            $id = $args['id'];
        }

        switch ($request->getMethod()) {
            case 'POST':
                return $this->create($request->getParsedBody());
                break;

            case 'GET':
                return $id ? $this->fetch($id) : $this->fetchAll();
                break;

            case 'PATCH':
                return $this->update($id, $request->getParsedBody());
                break;

            case 'DELETE':
                return $this->delete($id);
                break;

            default:
                return $this->notImplementedError();
        }
    }

    /**
     * @param array|object|null $data
     * @return Response
     */
    public function create($data)
    {
        return $this->notImplementedError();
    }

    /**
     * @param string $id
     * @return Response
     */
    public function fetch($id)
    {
        return $this->notImplementedError();
    }

    /**
     * @return Response
     */
    public function fetchAll()
    {
        return $this->notImplementedError();
    }

    /**
     * @param string $id
     * @param array|object|null $data
     * @return Response
     */
    public function update($id, $data)
    {
        return $this->notImplementedError();
    }

    /**
     * @param string $id
     * @return Response
     */
    public function delete($id = null)
    {
        return $this->notImplementedError();
    }

    /**
     * The default for all methods that aren't explicitly implemented by the resource
     *
     * @return Response
     */
    protected function notImplementedError()
    {
        return $this->response->withJson(['error' => 'Method not implemented'], 501);
    }

    /**
     * Can be used to explicitly specify that a method is not allowed for a particular resource
     *
     * @return Response
     */
    protected function notAllowedError()
    {
        return $this->response->withJson(['error' => 'Method not allowed'], 405);
    }
}
